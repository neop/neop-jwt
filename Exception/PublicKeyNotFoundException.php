<?php

namespace Neop\Jwt\Exception;

class PublicKeyNotFoundException extends \Exception
{
}
