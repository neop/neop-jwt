<?php

namespace Neop\Jwt\Security\Authenticator;

use Lcobucci\JWT\Token;
use Neop\Jwt\Token\Extractor\ExtractorInterface;
use Neop\Jwt\Token\Verifier\VerifierInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class JWTTokenAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var ExtractorInterface
     */
    private $extractor;
    /**
     * @var VerifierInterface
     */
    private $verifier;

    public function __construct(ExtractorInterface $extractor, VerifierInterface $verifier)
    {
        $this->extractor = $extractor;
        $this->verifier = $verifier;
    }

    public function start(Request $request, AuthenticationException $authException = null): JsonResponse
    {
        return new JsonResponse(['error' => 'Missing jwt token'], JsonResponse::HTTP_UNAUTHORIZED);
    }

    public function supports(Request $request): bool
    {
        return null !== $request->headers->get('Authorization');
    }

    public function getCredentials(Request $request)
    {
        /** @var Token $token */
        $token = $this->extractor->extract($request);
        if (!$token instanceof Token) {
            throw new \UnexpectedValueException('Invalid Token');
        }

        return $token;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (!$credentials instanceof Token) {
            throw new \InvalidArgumentException(sprintf('The credentials must be an instance of %s', Token::class));
        }

        /** @var Token $payload */
        $payload = $credentials;
        if (!$payload->hasClaim('id')) {
            throw new AuthenticationException('No ID found in the payload');
        }

        return $userProvider->loadUserByUsername($credentials);
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        if (!$credentials instanceof Token) {
            throw new \InvalidArgumentException(sprintf('The credentials must be an instance of %s', Token::class));
        }

        return $this->verifier->verify($credentials);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): JsonResponse
    {
        return new JsonResponse(['error' => $exception->getMessage()], JsonResponse::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        return null;
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
