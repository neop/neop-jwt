<?php

namespace Neop\Jwt\Token\Verifier;

interface VerifierInterface
{
    public function verify($token): bool;
}
