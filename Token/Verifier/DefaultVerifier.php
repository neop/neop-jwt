<?php

namespace Neop\Jwt\Token\Verifier;

use Lcobucci\JWT\Signer;
use Lcobucci\JWT\Token;
use Neop\Jwt\Exception\PublicKeyNotFoundException;

class DefaultVerifier implements VerifierInterface
{
    /**
     * @var Signer
     */
    private $signer;
    /**
     * @var false|string
     */
    private $publicKey;

    public function __construct(string $publicKeyPath, ?Signer $signer = null)
    {
        $this->publicKey = file_get_contents($publicKeyPath);
        if (!$this->publicKey) {
            throw new PublicKeyNotFoundException('Public key not found');
        }

        $this->signer = $signer ?? new Signer\Rsa\Sha256();
    }

    public function verify($token): bool
    {
        if (!$token instanceof Token) {
            throw new \InvalidArgumentException(sprintf('Token must be an instance of %s', Token::class));
        }

        return $token->verify($this->signer, $this->publicKey);
    }
}
