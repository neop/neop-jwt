<?php

namespace Neop\Jwt\Token\Extractor;

use Symfony\Component\HttpFoundation\Request;

interface ExtractorInterface
{
    public function extract(Request $request);
}
