<?php

namespace Neop\Jwt\Token\Extractor;

use Lcobucci\JWT\Token;
use Neop\Jwt\Token\Parser\JwtParserInterface;
use Symfony\Component\HttpFoundation\Request;

class HeaderExtractor implements ExtractorInterface
{
    public const HEADER_KEY = 'Authorization';
    public const PREFIX = 'Bearer';
    /**
     * @var JwtParserInterface
     */
    private $parser;
    /**
     * @var string
     */
    private $headerKey;
    /**
     * @var string
     */
    private $prefix;

    public function __construct(JwtParserInterface $parser, ?string $headerKey = null, ?string $prefix = null)
    {
        $this->parser = $parser;
        $this->headerKey = $headerKey ?? self::HEADER_KEY ;
        $this->prefix = $prefix ?? self::PREFIX;
    }

    public function extract(Request $request): ?Token
    {
        $headers = $request->headers;
        $authorizationValue = $headers->get($this->headerKey);
        if (null === $authorizationValue || 0 !== strpos($authorizationValue, $this->prefix)) {
            return null;
        }

        $jwtExtracted = explode(' ', $authorizationValue);
        if (count($jwtExtracted) !== 2) {
            return null;
        }

        $token = trim($jwtExtracted[1]);

        return $this->parser->parse($token);
    }
}
