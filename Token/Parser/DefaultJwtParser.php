<?php

namespace Neop\Jwt\Token\Parser;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Token;

class DefaultJwtParser implements JwtParserInterface
{
    /**
     * @var Parser
     */
    private $parser;

    public function __construct(?Parser $parser = null)
    {
        $this->parser = $parser ?? new Parser();
    }

    public function parse(string $token): Token
    {
        return $this->parser->parse($token);
    }
}
