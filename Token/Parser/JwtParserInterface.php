<?php

namespace Neop\Jwt\Token\Parser;

interface JwtParserInterface
{
    public function parse(string $token);
}
